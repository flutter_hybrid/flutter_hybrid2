import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

///  参考链接 https://www.jianshu.com/p/a6fdb5e4cc44
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //创建通道对象，与android端创建的通道对象名称一致
  MethodChannel _channel = MethodChannel("plugins.flutter.io/channel_name_1");
  //安卓端的按钮被点击的次数
  String _androidButtonClickedNumber = '0';
  //当前Flutter端按钮被点击的次数
  int _flutterButtonClickedNumber = 0;

  @override
  void initState() {
    super.initState();
    //设置此通道上的监听
    _channel.setMethodCallHandler(_handlerMethodCall);
  }

  Future<dynamic> _handlerMethodCall(MethodCall call) async {
    //获取通道监听中调用的函数名称
    String method = call.method;
    if (method == 'addAndroidButtonAndNoticeFlutter') {
      String androidButtonClickedNumber =
          call.arguments['AndroidButtonClickedNumber'];
      _androidButtonClickedNumber = androidButtonClickedNumber;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
              child: AndroidView(
            //与 Android 原生交互时唯一标识符,与Android端有对应关系
            viewType: 'plugins.flutter.io/android_view',
          )),
          Container(
            height: 200,
            width: double.infinity,
            color: Colors.red,
            child: Column(
              children: <Widget>[
                Text('flutter页'),
                SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                  onPressed: () {
                    _flutterButtonClickedNumber =
                        _flutterButtonClickedNumber + 1;
                    setState(() {});
                    Map<String, String> map = {
                      'FlutterButtonClickedNumber':
                          _flutterButtonClickedNumber.toString()
                    };
                    //在Flutter端调用执行函数，将Flutter端按钮的点击次数传递到安卓端
                    _channel.invokeMethod(
                        "addFlutterButtonAndNoticeAndroid", map);
                  },
                  child: Text(
                    "+1",
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                ),
                Text('android的按钮被点击数量：$_androidButtonClickedNumber'),
              ],
            ),
          )
        ],
      ),
    );
  }
}
